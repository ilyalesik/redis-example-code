import dotProp from 'dot-prop-immutable'
import createScheduleCommonReducer, {scheduleDefaultState} from './schedule_common'
import _ from 'underscore'

function getDefaultState() {
    return {
        ...scheduleDefaultState(),
        currentDoctor: {},
        otherDoctors: {}
    };
}

function setCurrentDoctor(state, currentDoctor) {
    return {...state, currentDoctor};
}

function setRandomOtherDoctors(state, doctorId, doctors) {
    if (state.otherDoctors[doctorId]) {
        return state;
    }
    return dotProp.set(state, `otherDoctors.${doctorId}`, doctors);
}

const scheduleReducer = createScheduleCommonReducer({typePrefix: 'DOCTOR'});

export default function(_state = getDefaultState(), action) {
    const state = scheduleReducer(_state, action);
    switch (action.type) {
        case 'SET_CURRENT_DOCTOR':
            return setCurrentDoctor(state, action.value);
        case 'SET_RANDOM_OTHER_DOCTORS':
            return setRandomOtherDoctors(state, action.doctorId, action.doctors)

    }
    return state;
}