import { createSelector } from 'reselect'
import _ from 'underscore'
import moment from 'moment'

const getCurrentCityId  = (state) => state.geo.currentCityId;
const getNews = (state) => state.news.news;
const getCurrentNews = (state) => state.news.currentNews;

const getFilterId = (state, props) => props.filterId;
const getLimit = (state, props) => props.limit;


const getNewsByCurrentCity = createSelector(
    [getCurrentCityId, getNews],
    (currentCityId, news) => {
        return _.chain(news)
            .filter((newsItem)=>newsItem.city === currentCityId)
            .map((newsItem)=> Object.assign({}, newsItem, {formattedDate: moment.utc(newsItem.date).locale('ru').format("DD MMMM YYYY")}))
            .value();
    }
);

export const getPinnedNews = createSelector(
    [getNewsByCurrentCity],
    (news) => {
        return _.find(news, (newsItem)=>newsItem.is_pinned);
    }
);

export const getUnpinnedNews = createSelector(
    [getNewsByCurrentCity, getFilterId],
    (news, filterId) => {
        return _.filter(news, (newsItem)=>!newsItem.is_pinned && (!filterId || newsItem.category.id === filterId));
    }
);

export const getPopupNews = createSelector(
    [getNews, getCurrentNews],
    (news, currentNews) => {
        return _.find(news, (newsItem)=>newsItem.id === currentNews);
    }
);