import webpack              from 'webpack';
import path                 from 'path';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';

export default function(app) {
    const config = {
        devtool: 'inline-source-map',
        devServer: {
            contentBase: './dist'
        },
        entry: [
            'babel-polyfill',
            'webpack-hot-middleware/client',
            './src/index.js'
        ],
        output: {
            filename: 'bundle.js',
            path: path.resolve(__dirname, 'dist'),
            publicPath: "/assets/"
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /(node_modules)/,
                    use: [
                        'react-hot-loader',
                        'babel-loader'
                    ]
                },
                {
                    test: /\.(css|scss)$/,
                    exclude: /(node_modules)/,
                    use: [
                        { loader: 'style-loader', options: { sourceMap: true } },
                        { loader: 'css-loader', options: { sourceMap: true } },
                        { loader: 'postcss-loader', options: { sourceMap: true } },
                        { loader: 'sass-loader', options: { sourceMap: true } },
                    ]
                },
                {
                    test: /\.(png|svg|jpg|gif|otf|ttf)$/,
                    use: [
                        {
                            loader: 'file-loader'
                        }

                    ]
                }
            ]
        },
        plugins: [
            new webpack.optimize.OccurrenceOrderPlugin(),
            new webpack.HotModuleReplacementPlugin(),
            new webpack.NoEmitOnErrorsPlugin()
        ]
    };

    const compiler = webpack(config);

    app.use(webpackDevMiddleware(compiler, {
        noInfo: true,
        publicPath: config.output.publicPath,
        stats: {colors: true}
    }));
    app.use(webpackHotMiddleware(compiler));
}