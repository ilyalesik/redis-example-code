import Immutable, {List, Map, fromJS} from 'immutable';
import {expect} from 'chai';

import buy from '../../src/reducers/buy';

describe('reducer buy', () => {

    it('handles SERVICE_SUCCESS simple', () => {
        var initialState = buy(Immutable.fromJS({}), {});


        const resultState = buy(initialState, {
            type: 'SERVICE_SUCCESS',
            value: [
                {
                    "id": 1,
                    "offers": [
                        {
                            "id": 1,
                            "count": 1,
                            "price": "600.00"
                        }
                    ],
                    "title": "Групповая тренировка 30'",
                    "description": ""
                }
            ]});

        expect(resultState.get('services').toJS()).to.deep.equal({
            1: {
                "id": 1,
                "offers": {
                    1: {
                        "id": 1,
                        "count": 1,
                        "price": 600.00
                    }
                },
                "title": "Групповая тренировка 30'",
                "description": ""
            }
        });
    });

    it('handles SERVICE_SUCCESS complex', () => {
        var initialState = buy(Immutable.fromJS({}), {});


        const resultState = buy(initialState, {
            type: 'SERVICE_SUCCESS',
            value: [
                {
                    "id": 1,
                    "offers": [
                        {
                            "id": 2,
                            "count": 3,
                            "price": "600.00"
                        },
                        {
                            "id": 3,
                            "count": 3,
                            "price": "1600.00"
                        }
                    ],
                    "title": "Групповая тренировка 30'",
                    "description": ""
                },
                {
                    "id": 4,
                    "offers": [
                        {
                            "id": 5,
                            "count": 6,
                            "price": "6500.00"
                        }
                    ],
                    "title": "Групповая тренировка 30'",
                    "description": ""
                }
            ]});

        expect(resultState.get('services').toJS()).to.deep.equal({
            1: {
                "id": 1,
                "offers": {
                    2: {
                        "id": 2,
                        "count": 3,
                        "price": 600.00
                    },
                    3: {
                        "id": 3,
                        "count": 3,
                        "price": 1600.00
                    }
                },
                "title": "Групповая тренировка 30'",
                "description": ""
            },
            4: {
                "id": 4,
                "offers": {
                    5: {
                        "id": 5,
                        "count": 6,
                        "price": 6500.00
                    }
                },
                "title": "Групповая тренировка 30'",
                "description": ""
            }
        });
    });


    it('handles BUY_CHANGE_SERVICE simple', () => {
        const initialState = buy(Immutable.fromJS({rows: [{
            service: undefined
        }]}), {});


        const resultState = buy(initialState, {
            type: 'BUY_CHANGE_SERVICE',
            serviceId: 333,
            rowId: 0
        });

        expect(resultState.get('rows').toJS()).to.deep.equal([{
            service: 333
        }]);
    });

    it('handles BUY_CHANGE_SERVICE update', () => {
        const initialState = buy(Immutable.fromJS({rows: [{
            service: 222
        },{
            service: 333
        },{
            service: 444
        }]}), {});


        const resultState = buy(initialState, {
            type: 'BUY_CHANGE_SERVICE',
            serviceId: 444,
            rowId: 1
        });

        expect(resultState.get('rows').toJS()).to.deep.equal([{
            service: 222
        },{
            service: 444
        },{
            service: 444
        }]);
    });

    it('handles BUY_CHANGE_OFFER update', () => {
        const initialState = buy(Immutable.fromJS({rows: [{
            service: 222
        },{
            service: 333
        },{
            service: 444
        }]}), {});


        const resultState = buy(initialState, {
            type: 'BUY_CHANGE_OFFER',
            offerId: 444,
            rowId: 1
        });

        expect(resultState.get('rows').toJS()).to.deep.equal([{
            service: 222
        },{
            service: 333,
            offer: 444,
        },{
            service: 444
        }]);
    });

    it('handles BUY_ADD_ROW simple', () => {
        const initialState = buy(Immutable.fromJS({rows: [{
            service: 333,
            offer: 444,
        }]}), {});


        const resultState = buy(initialState, {
            type: 'BUY_ADD_ROW',
        });

        expect(resultState.get('rows').toJS()).to.deep.equal([{
            service: 333,
            offer: 444
        }, {
            service: undefined
        }]);
    });

    it('handles BUY_REMOVE_ROW simple', () => {
        const initialState = buy(Immutable.fromJS({rows: [{
            service: 333,
            offer: 666,
        }, {
            service: 444,
            offer: 777,
        }, {
            service: 555,
            offer: 777,
        }]}), {});


        const resultState = buy(initialState, {
            type: 'BUY_REMOVE_ROW',
            rowKey: 1
        });

        expect(resultState.get('rows').toJS()).to.deep.equal([{
            service: 333,
            offer: 666,
        }, {
            service: 555,
            offer: 777,
        }]);
    });

    
});